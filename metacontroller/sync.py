from http.server import BaseHTTPRequestHandler, HTTPServer
import json


class Controller(BaseHTTPRequestHandler):
    def sync(self, parent, children):
        print("in sync method")
        # Compute status based on observed state.
        desired_status = {
            "services": len(children["Service.v1"]),
            "ingress": len(children["Ingress.networking.k8s.io/v1"]),
        }
        print("desired status", desired_status)
        print("allocation id:: ", parent["metadata"]["labels"]["determined"])
        # Generate the desired child object(s).
        # who = parent.get("spec", {}).get("who", "World")
        # parent_label = parent["metadata"]["labels"]["customlabel"]
        # if parent_label == "gpu-label":
        desired_service ={
                "apiVersion": "v1",
                "kind": "Service",
                "metadata": {
                    "name": "fiftyone-service-"+parent["metadata"]["labels"]["determined"][:-2], 
                    "namespace": "determined"},
                "spec": {
                    "selector": {
                        "determined": parent["metadata"]["labels"]["determined"]
                    },
                    "ports": [{"port": 5151, "targetPort": 5151}],
                },
            }
        desired_ingress= {
                "apiVersion": "networking.k8s.io/v1",
                "kind": "Ingress",
                "metadata": {"name": "fiftyone-ingress", "namespace": "determined"},
                "spec": {
                    "ingressClassName": "nginx",
                    "rules": [
                        {
                            "host": "fiftyone.az.kapernikov.com",
                            "http": {
                                "paths": 
                                [
                                    {
                                        "path": "/fiftyone-"+str(desired_status["ingress"]+1),
                                        "pathType": "Prefix",
                                        "backend": {
                                            "service": {
                                                "name": "fiftyone-service-"+parent["metadata"]["labels"]["determined"][:-2],
                                                "port": {"number": 5151}
                                            }
                                        }
                                    }
                            ]
                            }
                            
                        }
                    ],
                },
            }

        # return {"status": desired_status, "attachments": [[desired_service], [desired_ingress]]}
        return {'attachments': [desired_service, desired_ingress]}

    def do_POST(self):
        # Serve the sync() function as a JSON webhook.
        observed = json.loads(self.rfile.read(int(self.headers.get("content-length"))))
        desired = self.sync(observed["object"], observed["attachments"])
        print("desired state:: ", desired)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(json.dumps(desired).encode())
        print("resources created")


print("inside sync.py")
HTTPServer(("", 80), Controller).serve_forever()
